"""
The routes module contains stuff that are used for debugging.
Usually you don't include in production or deployment
"""
import flask
#	This is a child route, so we don't specify url_prefix like the previous one
router = flask.Blueprint("debug", __name__)


@router.get("/")
def page_debug():
	return flask.redirect("/debug/site-map")

@router.get("/site-map")
def page_sitemap():
	"""
	This route displays the entire site map of the server. Use this to debug errors of
	`Cannot GET/POST...etc  XXXXXX`
	:return:
	"""
	links:  list  = []
	server: flask.Flask = flask.current_app
	rule:   flask.Flask.url_rule_class = None
	logger = server.logger

	for rule in server.url_map.iter_rules():
		defaults = rule.defaults  or {}
		args     = rule.arguments or {}
		#	Skip things that cannot be reached
		if (len(defaults) < len(args)):
			continue

		url = flask.url_for(rule.endpoint, **defaults)

		links.append({
			"methods": rule.methods,
			"url":    url
		})
	return flask.render_template("./debug/sitemap.html", routes=links)