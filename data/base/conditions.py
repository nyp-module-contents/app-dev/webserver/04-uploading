"""
This module contains implementation of Conditions to be used with Rows
"""
from re              import search as regex_search
from typing          import Any

class Condition:
	"""
	Condition to match a Row.
	Performs Operation (column, params).
	Operations are implemented via inheritance.
	"""
	def __init__(self, attribute: str, *params: Any):
		"""
		Constructor
  
		Args:
			attribute (str):        The name of the variable / property to test against with
			params    (List[Any]):  N number of parameters as required
		"""
		self.__attribute = attribute
		self.__params    = params
	
	@property
	def _attribute(self):
		return self.__attribute
	
	@property
	def _params(self):
		return self.__params

	def verify(self, row: object) -> bool:
		if not hasattr(row, self._attribute):
			raise LookupError(f"Invalid parameter specified {self._attribute} doesn't exist in {row.__class__.__name__}")
		return True

class ConditionAny(Condition):
	"""
	Performs A or B or C or D == B
	"""
	def __init__(self, attribute: str, *params: Any):
		"""
		Constructor
		Args:
			attribute (str):       target to compare
			params    (list[Any]): List of columns to compare
		"""
		super().__init__(attribute, *params)

	def verify(self, row: object) -> bool:
		for p in self._params:
			assert(hasattr(row, p))
		subjects = list(map(lambda p: getattr(row, p), self._params))
		return self._attribute in subjects

class ConditionEQ(Condition):
	"""
	Performs A == B, A all of B
	"""
	def __init__(self, attribute: str, *params: Any):
		"""
		Constructor
		Args:
			attribute (str):       Column name or attribute name of row
			params    (list[Any]): List of targets (OR)
		"""
		super().__init__(attribute, *params)

	def verify(self, row: object) -> bool:
		super().verify(row)
		subject = getattr(row, self._attribute)
		for p in self._params:
			if subject != p:
				return False
		return True

class ConditionNE(ConditionEQ):
	"""
	Performs A != B, A neither all of B
	"""
	def __init__(self, attribute: str, *params: Any):
		"""
		Constructor
		Args:
			attribute (str):       Column name or attribute name of row
			params    (list[Any]): List of targets (OR)
		"""
		super().__init__(attribute, *params)

	def verify(self, row: object) -> bool:
		return not super().verify(row)

class ConditionOR(Condition):
	"""
	Performs A || B, A is any of B
	"""
	def __init__(self, attribute: str, *params: Any):
		super().__init__(attribute, *params)
  
	def verify(self, row: object) -> bool:
		return super().verify(row) and getattr(row, self._attribute) in self._params
	
class ConditionNOR(ConditionOR):
	"""
	Performs !(A || B), A is neither of B
	"""
	def __init__(self, attribute: str, *params: Any):
		super().__init__(attribute, *params)
  
	def verify(self, row: object) -> bool:
		return not super().verify(row)

class __ConditionCMP(Condition):
	"""
	Performs binary op
	"""
	def __init__(self, attribute: str, *params: Any):
		"""
		Constructor
  
		Args:
			attribute (str):       column or property
			params    (List[Any]): target to use. One only
  		"""
		assert(len(params) == 1)
		super().__init__(attribute, *params)

	def _cmp(self, row: object)  -> int:
		"""
		Compare the parameter
  
		Args:
			row (object): The row to compare against with

		Returns:
			int: differences
		"""
		return getattr(row, self._attribute) - self._params[0]

	def verify(self, row: object) -> bool:
		return super().verify(row)

class ConditionGT(__ConditionCMP):
	"""
	Performs A > B
 	"""
	def __init__(self, attribute: str, *params: Any):
		super().__init__(attribute, *params)
  
	def verify(self, row: object) -> bool:
		return super().verify(row) and self._cmp(row) > 0

class ConditionGE(__ConditionCMP):
	"""
	Performs A >= B
 	"""
	def __init__(self, attribute: str, *params: Any):
		super().__init__(attribute, *params)
  
	def verify(self, row: object) -> bool:
		return super().verify(row) and self._cmp(row) >= 0

class ConditionLT(__ConditionCMP):
	"""
	Performs A < B
 	"""
	def __init__(self, attribute: str, *params: Any):
		super().__init__(attribute, *params)
  
	def verify(self, row: object) -> bool:
		return super().verify(row) and self._cmp(row) < 0

class ConditionLE(__ConditionCMP):
	"""
	Performs A <= B
 	"""
	def __init__(self, attribute: str, *params: Any):
		super().__init__(attribute, *params)
  
	def verify(self, row: object) -> bool:
		return super().verify(row) and self._cmp(row) <= 0
	
class ConditionREGEX(Condition):
	"""
	Performs A REGEXP B.
	Compare A with regular expression and see any matches
 	"""
	def __init__(self, attribute: str, *params: Any):
		"""
		Constructor
  
		Args:
			attribute (str):       column or property
			params    (List[Any]): target to use. One only
		"""
		assert(len(params) == 1)
		super().__init__(attribute, *params)
  
	def verify(self, row: object) -> bool:
		return super().verify(row) and regex_search(self._params[0], str(getattr(row, self._attribute))) is None
		