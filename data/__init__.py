

def initialize_essential_data():
	"""
	Initialize data with essential information such as root access
	"""
	from uuid       import UUID
	from data.users import User, Users, UserRole
 
	with Users.open() as handle:
		if not Users.contains(handle, str(UUID(int=0))):
			Users.insert(handle, User(UserRole.ADMIN, "root", "P@ssw0rd", str(UUID(int=0))))
		else:
			user_root = Users.retrieve(handle, str(UUID(int=0)))
			user_root.set_password("P@ssw0rd")
			Users.update(handle, user_root)


def initialize_test_data():
	"""
	Initialize data for testing.
	"""
	from data.products import Product, Products
	import random
	with Products.open() as handle:
		if Products.size(handle) == 0:
			for i in range(0, 101):
				p       = Product()
				p.name  = f"Product {i:03d}"
				p.price = random.randint(0, 10) * 10 + random.choice([5, 10, 20, 50])
				p.description = f"Random description {i:03d}"
				Products.insert(handle, p)